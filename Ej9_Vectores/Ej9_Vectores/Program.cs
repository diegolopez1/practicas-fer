﻿using System;

namespace Ej9_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {


            string palabra2 = Console.ReadLine();
            int cantidad = palabra2.Length;
            int posicion = 0;
            int i = 0;
            int longitudSubstring = 1;
            string palabraFinal;
            int longitudCapicua = 0;
            int esCapi = 0;


            string[] array = new string[palabra2.Length];

            while (posicion < cantidad)
            {
                //Parametros substring: "longitudSubstring" es la longitud que va a tener el 
                //substring a partir del punto desde donde lo queres recortar, que lo indica "posicion"
                //Si a longitudSubstring le indicas 1, el string tendra un solo caracter, desde la posición.

                palabraFinal = palabra2.Substring(posicion, longitudSubstring);
                Console.WriteLine(palabraFinal);
                array[i] = palabraFinal;
                longitudCapicua = palabra2.Length - 1;
                posicion++;
                i++;
            }
            i = 0;
            while (i != longitudCapicua) 
                {
                                    
                if (array[i] != array[longitudCapicua]) 
                   {
                    Console.WriteLine("no es capicua");
                    esCapi++;
                    break;
                    }
                i++;
                longitudCapicua--;
                
            }
            if (esCapi == 0) Console.WriteLine("es capi");
        }
    }
}
