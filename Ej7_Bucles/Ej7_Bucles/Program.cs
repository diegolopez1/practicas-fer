﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej7_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("defina la cantidad de numeros que ingresará");

            decimal cantidad = int.Parse(Console.ReadLine());

            decimal suma = 0;
            Console.WriteLine("ahora, ingrese los numeros que desee, recuerde, serán " + cantidad + " numeros");

            for (int i = 0; i < cantidad; i++) 
            {
                decimal numero = decimal.Parse(Console.ReadLine());
                suma = suma + numero;

            }
            decimal promedio = suma / cantidad;
            Console.WriteLine("su promedio es " + promedio);

        }
    }
}
