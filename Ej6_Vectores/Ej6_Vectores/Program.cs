﻿using System;

namespace Ej6_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {

            /*6) Hacer un Programa que nos rellene de forma aleatoria un vector de 10 posiciones
            con números del 0 al 20, después tendremos 3 intentos para adivinar un número,
            cuando adivinemos un números nos indicará las posiciones en las que se
            encuentra.*/
            int salida = 20;
            int[] vector = new int[10];
            int i = 0;
            int numeroRandom;
            while (i < 10)
            {
                Random miRandom = new Random();
                numeroRandom = miRandom.Next(20);
                vector[i] = numeroRandom; 
                i++;
            }
            Console.WriteLine("ahora, elija tres numeros y si elige uno que haya salido elegido, le indicaremos la posición del mismo en el array");
            
            int eleccionUno = int.Parse(Console.ReadLine());

           
            
            for (i = 0; i < 10; i++) 
            {
                if ( vector[i] == eleccionUno)
                {
                    Console.WriteLine("has acertado, en la posición " + i);
                    i = 10;
                    salida = 20;
                }
            }
            if (salida != 20) Console.WriteLine("no acertaste, intentá de vuelta");
            
            int eleccionDos = int.Parse(Console.ReadLine());
            for (i = 0; i < 10; i++)
            {

                if (vector[i] == eleccionDos)
                {
                    Console.WriteLine("has acertado, en la posición " + i);
                    i = 10;
                }
            }

            if (i != 11) Console.WriteLine("no acertaste, intentá de vuelta");

            int eleccionTres = int.Parse(Console.ReadLine());

            for (i = 0; i < 10; i++)
            {
                
                if (vector[i] == eleccionTres)
                {
                    Console.WriteLine("has acertado, en la posición " + i);
                    i = 10;
                }


            }
            if (i != 11) Console.WriteLine("no acertaste");

        }
    }
}
