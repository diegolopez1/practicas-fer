﻿using System;

namespace Ej23_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int numero;

            do {
                Console.WriteLine("ingrese su edad y le indicaremos qué es, si ingresa cero sale del programa");
                numero = int.Parse(Console.ReadLine());
                if (numero <= 0 || numero > 99) 
                {
                    Console.WriteLine("el numero debe ser minimo 1, maximo 99");
                }
                    
                if (numero >= 1 && numero <= 12) Console.WriteLine("es un niño");
                if (numero > 12 && numero <= 21) Console.WriteLine("adolescente");
                if (numero > 21 && numero <= 69) Console.WriteLine("es un adulto");
                if (numero > 69 && numero < 99) Console.WriteLine("tercera edad");

            } while (numero != 0);    
            
            
        }
    }
}
