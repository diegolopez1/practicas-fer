﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej27_Bucle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Contar el número de números pares que introduzca el usuario y el número de
            //impares.Terminar cuando el usuario introduzca el número 100.


            int b;
            int a = 0;
            int pares = 0;
            int impares = 0;
            while (a != 100)
            {
                a = int.Parse(Console.ReadLine());
                if (a == 100) break;
                b = a % 2;
                if (b == 0) 
                {
                    pares++;
                }
                if (b != 0) 
                {
                    impares++;
                }

            } 

            Console.WriteLine("cantidad de impares " + impares + " cantidad de pares " + pares);


        }
    }
}
