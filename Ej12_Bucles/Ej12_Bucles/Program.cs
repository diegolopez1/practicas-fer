﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej12_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int suma = 0;
            int adaptacion = 0;
            for (int i = 10; i > -1 ; i-- ) 
            {
                adaptacion = i * 10;
                suma = suma + adaptacion;
                Console.WriteLine($"Valor de i: {i}. Valor de la adaptación: {adaptacion}. Valor de la suma: {suma}");

                
            }

        }
    }
}
