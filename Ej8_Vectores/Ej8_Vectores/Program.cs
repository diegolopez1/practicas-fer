﻿using System;

namespace Ej8_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*Escribir un programa que lea un vector de 10 elementos. Deberá imprimir el
mismo vector por pantalla pero invertido. Ejemplo: dado el vector 1 2 3 4 5 6 7 8 9
10 el programa debería imprimir 10 9 8 7 6 5 4 3 2 1.*/
            int[] arrayUno = new int[10];

            for (int i = 0; i < arrayUno.Length; i++) 
            {
                arrayUno[i] = int.Parse(Console.ReadLine());
            }

            for (int i = 9; i > -1; i--)
            {
                Console.WriteLine(arrayUno[i]);
            }

        }
    }
}
