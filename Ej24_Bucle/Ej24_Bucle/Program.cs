﻿using System;

namespace Ej24_Bucle
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("se sumarán los numeros que ingrese, al ingresar cero, el programa terminará");
            int numero;
            int suma = 0;
            do { Console.WriteLine("ingrese un numero para sumarlo, o cero para terminar"); 
            
                numero = int.Parse(Console.ReadLine());
                suma = suma + numero;
            
            
            
            }while (numero != 0);

            Console.WriteLine("la suma de sus numeros es " + suma);
        }
    }
}
