﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej13
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese en KG su peso y en Mts su altura");

            double peso = double.Parse(Console.ReadLine());
            double altura = double.Parse(Console.ReadLine());
            double imc = peso / (altura * altura);

            Console.WriteLine("tu indice es " + imc);
            if (imc < 18) Console.WriteLine("desnutride");
            if (imc >= 18 && imc >= 24.9) Console.WriteLine("normal");
            if (imc >= 25 && imc <= 26.9) Console.WriteLine("gordito");
            if (imc >= 27 && imc <= 29.9) Console.WriteLine("gordo");
            if (imc >= 30 && imc <= 39.9 ) Console.WriteLine("descontrolado");
            if (imc >= 40) Console.WriteLine("descontroladisimo");
        }
    }
}
