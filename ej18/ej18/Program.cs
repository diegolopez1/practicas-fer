﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej18
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("ingrese el nro de kilometros recorridos");

            int kmRecorridos = int.Parse(Console.ReadLine());
            int valorAPagar = 10000;
            
            if (kmRecorridos <= 400) Console.WriteLine("$"+valorAPagar);

            if (kmRecorridos > 400)
            {
                int b = (kmRecorridos - 370) / 30;
                int c = b * 100;
                valorAPagar = valorAPagar + c;
                
                if (kmRecorridos < 2000) Console.WriteLine("$"+valorAPagar); 

                else if (kmRecorridos >= 2000) 
                {
                    valorAPagar = valorAPagar + 250;
                    Console.WriteLine("$" + valorAPagar);
                }
            }

        }
    }
}
