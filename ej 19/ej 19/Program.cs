﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej_19
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("ingrese el sueldo del trabajador con dos decimales");

            decimal sueldo = decimal.Parse(Console.ReadLine());
            
            Console.WriteLine("sueldo " + sueldo);

            if (sueldo < 15000)
            {
                decimal sueldoConIncremento= sueldo * 1.15m;
                Console.WriteLine("sueldo ingresado: " + sueldo.ToString("0.00") + " sueldo final: " + sueldoConIncremento.ToString("0.00") + " porcentaje de aumento: 15%");
            }
            if (sueldo >= 15000 && sueldo <= 25000) 
            {
                decimal sueldoConIncremento = sueldo * 1.105m;
                Console.WriteLine("sueldo ingresado: " + sueldo.ToString("0.00") + " sueldo final: " + sueldoConIncremento.ToString("0.00") + " porcentaje de aumento: 10.5%");
            }
            if (sueldo > 25000) 
            {
                decimal sueldoConIncremento = sueldo * 1.08m;
                Console.WriteLine("sueldo ingresado: " + sueldo.ToString("0.00") + " sueldo final: " + sueldoConIncremento.ToString("0.00") + " porcentaje de aumento: 8%");
            } 




        }
    }
}
