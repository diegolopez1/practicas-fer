﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej26_Bucles
{
    internal class Program
    {   
        static void Main(string[] args)
        {
            //Multiplicar los números introducidos por el usuario que estén en el rango (1,10).
            //Terminar cuando el usuario introduzca un número fuera del rango y mostrar el
            //resultado.

            Console.WriteLine("los numeros que ingrese se multiplicaran, pero si se sale del rango de entre 1 y 10 el programa finalizará");
            int numero = 1;
            int multiplicador = 1;
            while (numero >= 1 && numero <= 10) 
            {
                numero = int.Parse(Console.ReadLine());
                if (numero > 10) 
                {
                    break;
                }
                multiplicador = multiplicador * numero;

            } 

            Console.WriteLine("sus numeros multiplicados dan: " + multiplicador);


        }
    }
}
