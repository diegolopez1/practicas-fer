﻿using System;

namespace Ej_16
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese los 3 lados del triangulo, le decimos que tipo es");

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            int c = int.Parse(Console.ReadLine());

            if (a == b && b == c && c == a) 
            {
                Console.WriteLine(" el triangulo es equilatero ");
            }

            else if (a != c && b != c && b != a)
            {
                Console.WriteLine(" el triangulo no es ni iscoceles ni equilatero");
            }

            else Console.WriteLine (" es isoceles");


        }
    }
}
