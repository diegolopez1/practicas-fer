﻿using System;

namespace Ej4__Funciones
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("indique a para suma, b para resta, c para mul, d para div, e para salir");

            string c = Console.ReadLine();
            switch (c) 
            {

                case "a":

                    Console.WriteLine("ingrese los dos numeros que quiere sumar");
                    int a = int.Parse(Console.ReadLine());
                    int b = int.Parse(Console.ReadLine());
                    Console.WriteLine(sumar(a, b));
                    break;

                case "b":
                    Console.WriteLine("ingrese los dos numeros que quiere restar");
                    int e= int.Parse(Console.ReadLine());
                    int f = int.Parse(Console.ReadLine());
                    Console.WriteLine(restar(e, f));
                    break;

                case "c":
                    Console.WriteLine("ingrese los dos numeros que quiere multiplicar");
                    int g = int.Parse(Console.ReadLine());
                    int h = int.Parse(Console.ReadLine());
                    Console.WriteLine(multiplicar(g, h));
                    break;

                case "d":
                    Console.WriteLine("ingrese los dos numeros que quiere dividir");
                    int i = int.Parse(Console.ReadLine());
                    int j = int.Parse(Console.ReadLine());
                    Console.WriteLine(dividir(i, j));
                    break;

                case "e":
                    break;

            }

        }


        static int sumar(int a, int b)
        {
            return a + b;
        }

        static int restar(int a, int b)
        {
            return a - b;
        }
        static int multiplicar(int a, int b)
        {
            return a * b;
        }
        static int dividir(int a, int b)
        {
            return a / b;
        }


    }
}
