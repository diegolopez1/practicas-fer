﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej4_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("favor de ingresar 20 numeros");

            decimal sumaPares = 0;
            decimal suma = 0;
            int pares = 0;
            int impares = 0;

            for (int i = 0; i < 20; i++)

            {

                decimal numero = decimal.Parse(Console.ReadLine());
                suma = suma + numero;
                
                if (numero % 2 == 0)
                {
                    sumaPares = sumaPares + numero; 
                    pares++;
                }

                if (numero % 2 != 0)
                {
                    impares++;
                }

            }
            
            decimal promedio = suma / 20;
            decimal promedioPares = sumaPares / pares;

            Console.WriteLine("el promedio es: " + promedio + ". Impares : " + impares + ". Pares :" + pares);
            Console.WriteLine("el promedio de los numeros pares es " + promedioPares);

        }
    }
}
