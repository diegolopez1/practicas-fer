﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej10
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("favor de ingresar las 4 notas de las 4 materias");
        
            int materiaA = int.Parse(Console.ReadLine());
            int materiaB = int.Parse(Console.ReadLine());
            int materiaC = int.Parse(Console.ReadLine());
            int materiaD = int.Parse(Console.ReadLine());

            int promedio = (materiaA + materiaB + materiaC + materiaD) / 4;

            Console.WriteLine("Su promedio es " + promedio);

            if (promedio < 4) Console.WriteLine("Promedio Insuficiente");

            if (promedio == 4) Console.WriteLine("Promedio Regular");

            if (promedio > 4 && promedio <= 6 ) Console.WriteLine("Promedio Bueno");

            if (promedio > 6 && promedio < 8) Console.WriteLine("Promedio Muy Bueno");

            if (promedio == 8 || promedio == 9) Console.WriteLine("Promedio Excelente");

            if (promedio == 10) Console.WriteLine("Promedio Sobresaliente");


        }
    }
}
