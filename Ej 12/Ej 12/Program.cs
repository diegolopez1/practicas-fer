﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej_12
{
    internal class Program
    {
        static void Main(string[] args)
        {
            
            Console.WriteLine("Favor de ingresar los grados F° para pasarlos a Celsius");
            double farenheit = double.Parse(Console.ReadLine());
            double celcius = (farenheit - 32) *0.55;
            Console.WriteLine("La temperatura es " + celcius + " grados celsius");

            if (celcius < -89.5) Console.WriteLine("la temp es menor a la menor registrada en la tierra");
            if (celcius == 0) Console.WriteLine("el agua se encuentra en estado solido");
            if (celcius > 0 && celcius <= 100) Console.WriteLine("agua en estado liquido");
            if (celcius > 100) Console.WriteLine("agua en estado gaseoso");
            if (celcius > 121) Console.WriteLine("supera la temperatura maxima que soporta la vida");
            

        }
    }
}
