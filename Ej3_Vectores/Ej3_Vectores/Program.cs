﻿using System;

namespace Ej3_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*2) Indicar si un número de 6 cifras ingresado por teclado es un número capicúa.
            Llenar un vector de 6 posiciones con valores enteros para resolver el problema.*/

            Console.WriteLine("ingrese un numero de 6 digitos");
            int numero = int.Parse(Console.ReadLine());
          
           

            int[] capicua = new int[6];
            capicua[0] = numero/100000;
            capicua[1] = (numero/10000) %10;
            capicua[2] = (numero/1000)% 10;
            capicua[3] = (numero/100) %10;
            capicua[4] = (numero/10) % 10;
            capicua[5] = numero % 10;

            foreach (int i in capicua) 
            {
                Console.WriteLine(i);
            }
            if (capicua[0] == capicua[5] && capicua[1] == capicua[4] && capicua[2] == capicua[3])
            {
                Console.WriteLine("es capicua");
            }
            else Console.WriteLine("no es capicua");





        }
    }
}
