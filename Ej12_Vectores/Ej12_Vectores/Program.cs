﻿using System;

namespace Ej12_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*12) Escribe un programa que lea del teclado una cadena y construya y muestre en la
pantalla otra cadena en la que cada vocal haya sido reemplazada por un punto.*/

            Console.WriteLine("ingrese una palabra");

            string palabra = Console.ReadLine();

            string[] array = new string[palabra.Length];

            for (int i = 0; i < palabra.Length; i++)
            {
                array[i] = palabra.Substring(i, 1);
                if (array[i] == "a" || array[i] == "e" || array[i] == "i" || array[i] == "o" || array[i] == "u")
                {
                    array[i] = ".";
                }
            }
            
            for (int i = 0; i < array.Length; i++)
            {

                Console.Write(array[i]);
            }

        }
    }
}
