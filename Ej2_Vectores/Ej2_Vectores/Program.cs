﻿using System;

namespace Ej2_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {/*2) Indicar si un número de 4 cifras ingresado por teclado es un número capicúa.
Llenar un vector de 4 posiciones con valores enteros para resolver el problema.*/

            Console.WriteLine("ingrese un numero de cuatro cifras");
            int[] capicua = new int[4];
            int numero = int.Parse(Console.ReadLine());

            capicua[0] = numero / 1000;
            capicua[1] = (numero / 100) % 10;
            capicua[2] = (numero / 10) % 10;
            capicua[3] = numero % 10;
                   
            foreach (int i in capicua) 
            {
                Console.WriteLine(i);
            }

            if (capicua[0] == capicua[3] && capicua[1] == capicua[2]) Console.WriteLine("es capicua");
            else Console.WriteLine("no es capicua");

        }
    }
}
