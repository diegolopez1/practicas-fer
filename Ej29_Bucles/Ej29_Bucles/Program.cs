﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej29_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*29) Escribir un programa, que permita:
1º) Ingresar por teclado dos números (datos enteros).
2º) Muestre por pantalla el mensaje:
La suma es <suma>
3º) Pregunte al usuario si desea realizar otra suma o no.
4º) Repita los pasos 1º, 2º y 3º, mientras que, el usuario no responda 'n' de (no).
5º) Muestre por pantalla la suma total de los números introducidos.*/


            int a;
            int b;
            int sumab = 0;
            string pregunta = "";
            do
            {

                Console.WriteLine("ingrese dos numeros");

                a = int.Parse(Console.ReadLine());
                b = int.Parse(Console.ReadLine());

                int suma = a + b;

                sumab = suma + sumab;

                Console.WriteLine("La suma total hasta ahora es de " + sumab + ", quiere realizar otra suma?");
                pregunta = Console.ReadLine();

            } while (pregunta != "n" || pregunta != "n");

            Console.WriteLine("La suma total es de " + sumab);
        }
    }
}
