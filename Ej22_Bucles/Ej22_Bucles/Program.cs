﻿using System;

namespace Ej22_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int i = 1;
            Console.WriteLine("ingrese numeros y le indicaremos cual es el mayor de todos los ingresados, para terminar con el programa ingrese 0");
            int numMasAlto = 0;
            while(i != 0) 
            {
                numMasAlto = i;
                i = int.Parse(Console.ReadLine());
                if (i == 0)
                {
                    i = 0;
                    Console.WriteLine("ingresaste cero, finaliza el programa");
                }

                if (numMasAlto < i) numMasAlto = i; 
            }   
            Console.WriteLine("el numero más alto ingresado es " + numMasAlto);
          
        }
    }
}
