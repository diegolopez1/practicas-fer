﻿using System;

namespace Ej3_Funciones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese los dos lados del triangulo, le indicaremos el área");


            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());
            
            Console.WriteLine(metodo(a, b));
        }

        static decimal metodo(int a, int b)
            {
                decimal area = a * b / 2;

                return area;
            }

    }
}
