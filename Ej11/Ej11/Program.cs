﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej11
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese el valor de los coeficientes de la ecuación a * x + b = 0; primero ingrese a, luego b");
            double a = double.Parse(Console.ReadLine());
            double b = double.Parse(Console.ReadLine());

            double x = (b * (-1) ) / a;

            if (x == 0) Console.WriteLine("x no puede ser cero");

            double ecuacion = a * x + b;

            Console.WriteLine("el valor de x es " + x + " la ecuación da como resultado " + ecuacion);



        }
    }
}
