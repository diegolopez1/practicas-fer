﻿using System;

namespace Ej7_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {   /*Generar 3 vectores de 20 posiciones cada uno. El vector A deberá contener
números aleatorios entre 0 y 99. El B, también deberá ser rellenado con valores
aleatorios entre 0 y 99, y el vector C deberá contener la suma de A + B en cada
posición. Al finalizar, el vector C además deberá indicar el promedio de todos los
números que tiene almacenado.*/

            int[] arrayUno = new int[20];
            int[] arrayDos = new int[20];
            int[] arrayTres = new int[20];

            for (int i = 0; i < arrayUno.Length; i++)
            {
                Random r = new Random();
                arrayUno[i] = r.Next(90);
                Console.WriteLine(arrayUno[i]);
            }

            for (int i = 0; i < arrayDos.Length; i++)
            {
                Random r = new Random();
                arrayDos[i] = r.Next(90);
                Console.WriteLine(arrayDos[i]);
            }
            int suma = 0;
            int promedio = 0;
            for (int i = 0; i < arrayTres.Length; i++)
            {
                arrayTres[i] = arrayUno[i] + arrayDos[i];
                suma = suma + arrayTres[i];
                Console.WriteLine("Array 1: " + arrayUno[i] + " Array 2: " + arrayDos[i] + " Array: 3 " + arrayTres[i]);
                
                if (i == 19){
                    promedio = suma / (i + 1);
                    Console.WriteLine("Total Suma " + suma);
                    Console.WriteLine("Promedio " + promedio);
                    }
            }

        }
    }
}
