﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej2_Funciones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*2) Hacer un Programa que permita calcular mediante el uso de funciones, SIN paso de
             parámetros, el área de un rectángulo cuyos lados serán ingresados por teclado.*/

            metodo();

        }

        static void metodo() 
        {
            Console.WriteLine("ingrese los dos lados del triangulo, le indicaremos el área");

            int a = int.Parse(Console.ReadLine());
            int b = int.Parse(Console.ReadLine());

            decimal area = a * b / 2;

            Console.WriteLine(area);
        }

    }
}
