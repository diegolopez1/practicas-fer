﻿using System;

namespace Ej20_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int a = int.Parse(Console.ReadLine());
            int i = 1;
            while (a >= 1)
            {
                i = 1;
                while (i <= 10) 
                {
                    
                    int resultadoa = a * i;
                    Console.WriteLine(i + " . " + a + " = " + resultadoa);
                    i++;
                }
                
                a--;
            
            }
            
        }
    }
}
