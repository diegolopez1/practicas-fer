﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej10_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            int iteraciones = 0;
            int suma = 0;
            for (int i = 0; i < 10; i++) 
            {
                iteraciones = i;

                int alCuadrado = iteraciones * iteraciones;
                suma = suma + alCuadrado;
                Console.WriteLine($"Numero de iteración: {i}. Al cuadrado: {alCuadrado}. Suma hasta ahora: {suma}");

            }

        }
    }
}
