﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ej20
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("ingrese el monto de su compra");
            decimal compra = decimal.Parse(Console.ReadLine());

            if (compra < 500) Console.WriteLine("su compra no cuenta con descuentos, total: " + compra + "$");

            if (compra >= 500 && compra <= 1000) 
            {
                compra = compra * 0.95m;
                Console.WriteLine("el total de su compra es de " + compra + "$");
            }

            if (compra > 1000 && compra <= 7000)
            {
                compra = compra * 0.89m;
                Console.WriteLine("el total de su compra es de " + compra + "$");
            }

            if (compra > 7000 && compra <= 15000)
            {
                compra = compra * 0.82m;
                Console.WriteLine("el total de su compra es de " + compra + "$");
            }
            if (compra > 15000)
            {
                compra = compra * 0.75m;
                Console.WriteLine("el total de su compra es de " + compra + "$");
            }
        }

    }
}
