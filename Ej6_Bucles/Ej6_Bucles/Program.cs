﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej6_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("indique de que numero quiere ver la tabla");
            int n = int.Parse(Console.ReadLine());
            

            for (int i = 0; i < 11; i++)
            {
                int total = i * n;
                Console.WriteLine( n + " * " + i + " = " + total);

            }


        }
    }
}
