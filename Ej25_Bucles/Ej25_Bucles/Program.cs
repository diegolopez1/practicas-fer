﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej25_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {

            Console.WriteLine("ingrese numeros, en caso de ingresar uno par, el programa finalizará, al final le indicaremos el promedio y la cantidad ingresada");
            int cantidad = 0;
            int calculo;
            int suma = 0;
            int promedio;
            do
            {
                cantidad++;
                int numero = int.Parse(Console.ReadLine());
                calculo = numero % 2;
                suma = suma + numero;
                promedio = suma / cantidad;

                Console.WriteLine("cantidad " + cantidad + " promedio " + promedio + " suma: " + suma);



            } while (calculo != 0);

            Console.WriteLine("TOTAL: cantidad " + cantidad + " promedio " + promedio + " suma: " + suma);

        }
    }
}
