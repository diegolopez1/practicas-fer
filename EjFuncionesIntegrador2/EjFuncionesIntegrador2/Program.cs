﻿using System;

namespace EjFuncionesIntegrador2
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*Hacer un programa para verificar si Gané el Quini 6. El programa deberá en primer lugar,
            permitirle al usuario, almacenar 6 números + 2 como Jack POT. Estos últimos dos números
            podrán ser valores entre 0 y 9 Una vez que estos valores estén almacenados, el programa
            deberá en forma aleatoria, generar 6 números entre 0 y 41 y 2 valores entre 0 y 9 para el
            JACK POT.
            El programa deberá imprimir todo en pantalla y comparar los números aleatorios VS los
            que jugué, además de decirme si gané algo o no. Las condiciones del juego son:
            GANO EL POZO!: si acerté todo.
            NO GANO NADA: si no acerté ninguno
            Gano 1000: si acerté sólo los 2 JackPOT
            Gano 10000: si acerté 4 numeros (que no son del JACKPOT)
            Gano 100000: si acerté 5
            Gano 1000000: si acerté 6
            CASO CONTRARIO, indicar que acerté.*/
            
            Random random = new Random();
            
        int acertadas1 = 0;
        int acertadasJackPot = 0;
            
        int[] arrayUsuario = new int[8];
        int[] arrayMaquina = new int[8];


        arrayUsuario = almacenarNumerosUsuario(arrayUsuario);    
        arrayMaquina = generarRandom(arrayMaquina);

        resultadoNumeros(arrayUsuario, arrayMaquina, acertadas1, acertadasJackPot);

        }

        //almacenar numeros usuario

        static int[] almacenarNumerosUsuario(int[] array) 
        {
            
        Console.WriteLine("ingrese 6 numeros hasta el 41 inclusive, si ingresa un numero fuera de ese rango, se ingresará el anterior numero ingresado +1, luego ingrese los ultimos dos numeros que seran de jackpot");
        for (int i = 0; i < 8; i++) 
        {
                int numero = int.Parse(Console.ReadLine());
                if (i > 5) 
                {
                    
                    if (numero < 1 || numero > 9)
                    {
                        numero = 1;
                    }
                    array[i] = numero;
                }

            if (numero < 1 || numero > 41)
            {
            numero = 1;
            }
            array[i] = numero; 
        }
        
            return array;
        }
        //generar array de Randoms
        static int[] generarRandom(int [] arrayRandom) 
        {
            Random numeroRandom = new Random();
            for (int i = 0; i < 8; i++) 
            {
                if (i > 5)
                {
                    arrayRandom[i] = numeroRandom.Next(1, 9);
                    Console.WriteLine(arrayRandom[i]);
                }
                if (i <= 5) 
                {                    
                    arrayRandom[i] = numeroRandom.Next(41);
                    Console.WriteLine(arrayRandom[i]);
                }
            }
            return arrayRandom;
        }

        // ganaste o no ganaste?

        static void resultadoNumeros(int[] array, int[] array2, int contador, int contador2) 
        {
            contador = 0;
            contador2 = 0;
            for (int i = 0; i < 6; i++)
            {
                if (array[i] == array2[i]) 
                {
                    Console.WriteLine("acertaste en la posición " + i + " el siguiente numero " + array[i]);
                    contador++;
                }
            for (i = 6; i < 8; i++)
                if (array[i] == array2[i])
                {
                    Console.WriteLine("acertaste, del jackpot, en la posición: " + i + ", el siguiente numero: " + array[i]);
                    contador2++;
                }
            
            Console.WriteLine("acertaste " + contador + " numeros basicos, y " + contador2 + " del jackpot");

            if(contador >= 4 || contador2 ==2) 
                {
                    if (contador == 6) Console.WriteLine("ganaste el Millon");
                    if (contador == 5) Console.WriteLine("ganaste el CienMil");
                    if (contador == 4) Console.WriteLine("ganaste el DiezMil.");
                    if (contador2 == 2) Console.WriteLine("1000 del JackPot");
                }
                if (contador < 4 && contador2 < 2) Console.WriteLine("no ganaste nada");
            
            }
        }




    }
}
