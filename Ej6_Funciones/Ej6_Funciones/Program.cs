﻿using System;

namespace Ej6_Funciones
{
    internal class Program
    {
        static void Main(string[] args)

            /* Hacer un programa que implemente funciones para realizar el cálculo del promedio de 3
valores del tipo Float que son introducidos por teclado*/
        {
            float a = 0;
            float b = 0;
            float c = 0;
            promedio(a, b, c);
        }

        static void promedio(float a, float b, float c)
        {
            a = float.Parse(Console.ReadLine());
            b = float.Parse(Console.ReadLine());
            c = float.Parse(Console.ReadLine());
            Console.WriteLine((a + b + c) / 3);
        }

    }
}
