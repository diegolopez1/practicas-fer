﻿using System;

namespace Ej5_Funciones
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /* Repetir el programa anterior, pero esta vez, considerando que los 3 valores serán números
            aleatorios entre 0 y 100. Implementar una función “aleatorio” para el cálculo del número,
            y una función promedio para encontrar el promedio del número.*/
            Random f = new Random();
            Random g = new Random();
            Random h = new Random();
            float a = f.Next(0,100);
            float b = g.Next(0, 100);
            float c = h.Next(0, 100);
            promedio(a, b, c);
            Console.WriteLine("a vale " + a + " b vale " + b + " c vale " + c);
            
        }

        static void promedio(float a, float b, float c) 
        {
         Console.WriteLine((a + b + c)/3);
        }
    }
}
