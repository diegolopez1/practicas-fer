﻿using System;

namespace Ej10_Funcion
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*10) Hacer un programa que permita realizar las siguientes acciones (con funciones), vinculadas
            al menú de opciones que deberá implementarse con SWITCH

            1) CARGAR VECTOR
            2) PONER VECTOR EN 0
            3) VISUALIZAR VECTOR
            4) PROMEDIO
            5) SALIR

            Cuando el usuario presione la opción 1, deberá cargarse en forma aleatoria, un vector de 20
            posiciones.
            Si ocurre que el usuario, no empieza el programa por la opción 1, el programa deberá indicar algún
            mensaje, por ejemplo “ Sr. USUARIO, UD no ha cargado el vector aún”. Deberá luego de presionar
            una tecla volver al menú principal ** Atención podría implementar una función “CHEQUEO” para
            verificar este punto.
            Si presiona el 2) el programa deberá poner todo el vector con 0s
            Si presiones el 3) Deberá visualizar el vector, esté como esté.
            Si presiona el 4) el programa deberá imprimir el vector y mostrar el promedio.*/

            int numero = 0;
            int[] vector = new int[20];
            int i = 0;
            int suma = 0;
            int promedio;
            int result = 0;
            
            do
            {
                Console.WriteLine($"1 para imprimir, 2 para que todos los valores sean 0, 3 para ver el vector, 4 para mostrar y promediar, 5 para salir");
                numero = int.Parse(Console.ReadLine());

                if (numero == 5) { 
                    Console.WriteLine("El programa finaliza aquí");
                    break;
                }
                if (numero == 1) 
                {
                    while (i < 20)
                    {
                        // de esta manera, no estas guardando los numeros en la lista, solo los estas imprimiendo uno por uno
                        // en la consola
                        //El error que te tira es porque queda incompleta la posición del array
                        int cantidadDeNumeros = vector.Length;
                        GenerarRandom(vector, cantidadDeNumeros);                                           
                        suma = suma + result;
                        Console.WriteLine(suma);
                        i++;
                    }
                    
                }
                if (numero == 2) 
                {
                    while (i < 20)
                    {
                        int vectorBorrado = BorrarVector(vector, i);
                        i++;
                    }
                }
                if (numero == 3) 
                {
                    while (i < 20) {
                    ImprimirVector(vector, i);
                    i++;
                    }
                }
                i = 0;
               
                if (numero == 4) 
                {
                    promedio = suma / 20;
                    Console.WriteLine(promedio);
                }

            } while (numero != 5);


        }
    
        static int GenerarRandom( int[]vector, int i) 
        {
            int randomeada = 0;
            Random numeroRandom = new Random();
            randomeada = numeroRandom.Next(1000);
            int [] array = new int [i];
            int lista = array[randomeada];
            return lista;
            
        }

        static int BorrarVector ( int[] cualquiera, int i) 
        {
            i = 0;
            cualquiera [i] = 0;
            Console.WriteLine(cualquiera);
            i++;
            return cualquiera [i];
        }

        static void ImprimirVector (int[]cualquiera, int i) 
        {
            int pirulo = cualquiera[i]; 
            Console.WriteLine(pirulo);
        }
    }

}
