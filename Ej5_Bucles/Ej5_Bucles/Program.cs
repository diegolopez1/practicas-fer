﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej5_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            Console.WriteLine("Tabla del 5");
            int b = 0;

            for (int i = 0; i < 11; i++)
            {
                int total = b * 5;
                Console.WriteLine("5 * " + b + " = " + total);
                b++;

            }


        }
    }
}
