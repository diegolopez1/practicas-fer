﻿using System;

namespace Ej11_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*11) Escribir un programa que lea del teclado una cadena y muestre en la pantalla la
cantidad de consonantes y de vocales que contiene.*/

            Console.WriteLine("ingrese una palabra");

            string palabra = Console.ReadLine();

            string[]array = new string[palabra.Length];

            int contadorVocales = 0;

            int contadorConsonantes = 0;

            for (int i = 0; i < palabra.Length; i++)
            {
                array[i] = palabra.Substring(i, 1);
                if (array[i] == "a" || array[i] == "e" || array[i] == "i" || array[i] == "o" || array[i] == "u")
                {
                    contadorVocales++;
                }
                else contadorConsonantes++;
                Console.WriteLine(array[i]);
            }
            Console.WriteLine("cantidad de vocales " + contadorVocales + " Cantidad de Consonantes: " + contadorConsonantes);

        }
    }
}
