﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej28_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {
            //Elabora un algoritmo tal que solicite palabras hasta que se introduzca la palabra ‘FIN’
            //o ‘fin’. Muestra cuántas palabras introdujo el usuario.

            string a = "";
            int contador = -1;

            while (a != "fin" && a != "FIN") 
            {
                a = Console.ReadLine();
               
                contador++;

            }
            
            Console.WriteLine("cantidad de palabras "+ contador);
        }
    }
}
