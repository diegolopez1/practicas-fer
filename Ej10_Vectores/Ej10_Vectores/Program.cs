﻿using System;

namespace Ej10_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
            /*10) Escribir un programa que lea 10 números por teclado. Luego lea dos más e indique
si éstos están entre los anteriores.*/
            
            int[]array = new int[10];
            int[]arrayDos = new int[2];
            Console.WriteLine("escriba 10 numeros");
            for (int i = 0; i < 10; i++)

            {
                array[i] = int.Parse(Console.ReadLine());
            }
            
            Console.WriteLine("Ahora agregue dos mas");

            for(int i = 0; i < 2; i++) 
            {
                arrayDos[i] = int.Parse(Console.ReadLine());
                
            }

            for(int i = 0; i < 10; i++) 
            
            {
                int posicionArray = i + 1;
                if (array[i] == arrayDos[0])
                {
                    Console.WriteLine("array 1, posición " + posicionArray + " coincide con el primer numero ingresado");
                }
                if (array[i] == arrayDos[1])
                {
                    Console.WriteLine("array 1, posición " + posicionArray + " coincide con el segundo numero ingresado");
                }


            }

        }
    }
}
