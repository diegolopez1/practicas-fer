﻿using System;

namespace EjFuncionesIntegrador
{
    internal class Program
    {
        static void Main(string[] args)
        {/*10) Hacer un programa que permita realizar las siguientes acciones (con funciones), vinculadas
al menú de opciones que deberá implementarse con SWITCH

1) CARGAR VECTOR
2) PONER VECTOR EN 0
3) VISUALIZAR VECTOR
4) PROMEDIO
5) SALIR

Trabajo Práctico N° 6 – Funciones en C

3
Cuando el usuario presione la opción 1, deberá cargarse en forma aleatoria, un vector de 20
posiciones.
Si ocurre que el usuario, no empieza el programa por la opción 1, el programa deberá indicar algún
mensaje, por ejemplo “ Sr. USUARIO, UD no ha cargado el vector aún”. Deberá luego de presionar
una tecla volver al menú principal ** Atención podría implementar una función “CHEQUEO” para
verificar este punto.
Si presiona el 2) el programa deberá poner todo el vector con 0s
Si presiones el 3) Deberá visualizar el vector, esté como esté.
Si presiona el 4) el programa deberá imprimir el vector y mostrar el promedio.*/








            int opcion = 0;


            int[] miArray = new int[20];
            do
            {
                opcion = int.Parse(Console.ReadLine());
                vectorVacio(opcion, miArray);



                switch (opcion)
                {
                    case 1:
                        miArray = generarRandoms(miArray);
                        break;

                    case 2:
                        miArray = vectorEnCero(miArray);
                        break;

                    case 3:
                        mostrarVector(miArray);
                        break;

                    case 4:
                        promedioVector(miArray);
                        break;

                }
            } while (opcion != 5);

        }

        static int[] generarRandoms(int[] array)
        {
            Random numeroRandom = new Random();
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = numeroRandom.Next(100);
                Console.WriteLine(array[i]);
            }
            return array;
        }

        static void vectorVacio(int opcion, int[] miArray)
        {
            if (opcion != 1 && miArray[0] == 0 && miArray[1] == 0 && miArray[2] == 0)
                Console.WriteLine("tenga en cuenta que no ha cargado aun ningun vector, para comenzar, presione 1 ");
        }

        static int[] vectorEnCero(int[] array)
        {
            for (int i = 0; i < array.Length; i++)
            {
                array[i] = 0;
            }
            foreach (int i in array)
            {
                Console.WriteLine(i);
            }
            return array;
        }

        static void mostrarVector(int[] array)
        {
            foreach (int i in array)
            {
                Console.WriteLine(i);
            }
        }

        static void promedioVector(int[] array)
        {
            int suma = 0;

            for (int i = 0; i < array.Length; i++)
            {
                suma = suma + array[i];
            }
            decimal promedio = suma / array.Length;

            Console.WriteLine("El promedio es: " + promedio);

        }

    }
}
