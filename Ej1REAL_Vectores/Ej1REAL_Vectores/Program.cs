﻿using System;

namespace Ej1REAL_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {

            /*Crear un programa que solicite el ingreso de N números. Los números deberán ser
            almacenados en un vector. Los mismos deberán mostrar pantalla pero al final, se
            deberá mostrar los siguientes resultados:
            a) La sumatoria de los números
            b) El promedio de los números ingresados
            c) El menor de los números que se ingresó, y en qué posición del vector se
            encuentra.*/

            Console.WriteLine("ingrese la cantidad de numeros que va a necesitar");

            int n = int.Parse(Console.ReadLine());
            int i = 0;
            int[] numeros = new int[n];
            int promedio = 0;
            int suma = 0;
            int menor = 1000000000;
            while (i < n) 
            {

                Console.WriteLine("ingrese el numero de la posición " + i);                
                numeros[i] = int.Parse(Console.ReadLine());
                suma = suma + numeros[i];
                if (menor > numeros[i]) 
                {
                    menor = numeros[i];
                    Console.WriteLine("valor del menor" + menor);
                }
                Console.WriteLine("resultado de la suma hasta ahora: " + suma);
                i++;
            }
            promedio = suma / n;
            Console.WriteLine("El promedio es " + promedio + ". El menor es " + menor);

            

        }
    }
}
