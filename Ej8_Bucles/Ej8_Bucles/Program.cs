﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Ej8_Bucles
{
    internal class Program
    {
        static void Main(string[] args)
        {


            Random aleatorio = new Random();
            decimal numero;
            decimal suma = 0;
            decimal cantidadIteraciones = 0;
            

            for(int i = 1; i < 51; i++) 
            {
                numero = aleatorio.Next(0, 1000);
                cantidadIteraciones = i;
                Console.WriteLine($"Posición 1: {i}. Numero aleatorio correspondiente: {numero}");

                suma = suma + numero;
            }

            decimal promedio = suma / cantidadIteraciones;
            Console.WriteLine($"El promedio es {promedio}");

        }
    }
}
