﻿using System;

namespace Ej5_Vectores
{
    internal class Program
    {
        static void Main(string[] args)
        {
        /*5) Hacer un programa similar al anterior, pero rellenar el vector con números entre 0
        y 1000. Esta vez, el programa debe imprimir en pantalla todos los números
        aleatorios e informar al final, la cantidad de PARES menores a 352.*/

            int[] vector = new int[5000];
            int i = 0;
            int contadorPares = 0;
            while (i < 5000) 
            {

                Random random = new Random();
                int numeroRandom = random.Next(1000);
                vector[i] = numeroRandom;
                int pares = numeroRandom % 2;
                if (pares != 0 && numeroRandom < 352) 
                {
                    contadorPares++;
                    Console.WriteLine(numeroRandom);
                }
                i++;
                            
            }
            Console.WriteLine("el total de pares menores a 352 es " + contadorPares+ " el total de numeros es " + i);

        }
    }
}
